const router = require("express").Router();

const requestHandler = require("../middleware/requestHandler");

const getAllController = require("../controller/user/getAll");

router.get("/all", getAllController.handler);

module.exports = router;

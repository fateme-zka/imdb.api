const Joi = require("joi");

const schema = Joi.object({});

const handler = async (req, res) => {
  return res.send("This is users page.");
};

module.exports = { handler, schema, auth: false };

const Base = require("./Base");

module.exports = (sequelize, DataTypes) => {
  return Base(sequelize, DataTypes, "movie", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    format_id: { type: DataTypes.INTEGER, allowNull: false },
    genre_id: { type: DataTypes.INTEGER, allowNull: false },
    description: { type: DataTypes.TEXT, allowNull: false },
    image: { type: DataTypes.STRING, allowNull: false },
    trailer: { type: DataTypes.STRING, allowNull: true },
    video: { type: DataTypes.STRING, allowNull: false },
    is_series: { type: DataTypes.BOOLEAN, allowNull: false },
    year: { type: DataTypes.INTEGER, allowNull: true },
    time: { type: DataTypes.STRING, allowNull: false },
  });
};

const Sequelize = require("sequelize");

module.exports = class Context {
  constructor() {
    // create d
    this.database = require("./database");
  }

  init() {
    // Models
    const User = require("./model/User");
    const Movie = require("./model/Movie");
    const Genre = require("./model/Genre");
    const Format = require("./model/Format");

    // Tables
    const user = User(this.database, Sequelize.DataTypes);
    const movie = Movie(this.database, Sequelize.DataTypes);
    const genre = Genre(this.database, Sequelize.DataTypes);
    const format = Format(this.database, Sequelize.DataTypes);

    // Set Foreign Keys
    movie.belongsTo(user, {
      foreignKey: { name: "country_id", allowNull: false },
    });

    // Creat tables in db
    this.database.sync({ force: false });
  }
};
